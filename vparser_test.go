
package view

import (
	"fmt"
 	"os"
	"time"
	"testing"

	"bitbucket.org/EScottDaniels/sketch"
)

var (
	gen_ps	bool
	common_gc sketch.Graphics_api
)

/*
	Generate a graphics context for either postscript or xi interface.
*/
func get_gc( ) ( sketch.Graphics_api, error ) {
	var (
		err error = nil
	)

	if common_gc == nil {
		if gen_ps {
			common_gc, err = sketch.Mk_graphics_api( "postscript", "sketch.ps" )
		} else {
			common_gc, err = sketch.Mk_graphics_api( "xi", ",900,1100,Test" )
		}
	}

	return common_gc, err
}

func set_var( vp *Vparser, vs string ) ( bool ) {

	fmt.Fprintf( os.Stderr, "[INFO] parsing var string (%s)\n", vs )
	s, _ := vp.Crunch_rec( vs )
	if s == "" {
		return false
	}

	if s == "bad-data" {
		fmt.Fprintf( os.Stderr, "[FAIL] bad data returned from crunchvar on: %s\n", vs )
		return false
	}

	fmt.Fprintf( os.Stderr, "[OK]   set variable returned ok string: %s\n", s )
	return true
}

func set_element( vp *Vparser, vs string ) ( bool ) {

	fmt.Fprintf( os.Stderr, "[INFO] parsing element string (%s)\n", vs )
	s, _ := vp.Crunch_rec( vs )
	if s != "" {
		fmt.Fprintf( os.Stderr, "json; %s\n", s )
		return true
	}

	fmt.Fprintf( os.Stderr, "[FAIL] set element returned nil string" )
	return false
}

func Test_vp_variable( t *testing.T ) {
	vp := Mk_vparser( 1.0 )

	if vp == nil {
		fmt.Fprintf( os.Stderr, "[FAIL] nil pointer returned by mk function\n" )
		t.Fail()
		return
	}

	
	if ! set_var( vp, "var esd E. Scott Daniels" ) { t.Fail() }
	if ! set_var( vp, "var foo bar" ) { t.Fail() }
	if ! set_var( vp, `var qfoo "quoted text   should    preserve     spaces"` ) { t.Fail() }
	if ! set_var( vp, "var tfoo unquoted text  should not  preserve    spaces" ) { t.Fail() }
	if ! set_var( vp, "var cfoo bar with trailing comment # comment is here" ) { t.Fail() }
	if ! set_var( vp, "var efoo boo = >>> $esd <<<< expansion" ) { t.Fail() }
	if ! set_var( vp, "var efoo2 boo = >>> ${esd} <<<< expansion" ) { t.Fail() }
	if ! set_var( vp, "var efoo3 boo = >>> ${esd}barwith <<< something >>> $qfoo <<<" ) { t.Fail() }
	if ! set_var( vp, "var efoo4 boo = >>> ${novar:-DEFAULT} << default?" ) { t.Fail() }
	if ! set_var( vp, "var efoo5 boo = >>> \\$no-expand \\${noexpand} <<< escaped un expanded?" ) { t.Fail() }
		
	vp.Dump_st()
}

func Test_vp_element( t *testing.T ) {
	vp := Mk_vparser( 1.0 )

	if vp == nil {
		fmt.Fprintf( os.Stderr, "[FAIL] nil pointer returned by mk function\n" )
		t.Fail()
		return
	}
	
	
	fmt.Fprintf( os.Stderr, "\n----------------------------------------\n" )
	if ! set_var( vp, "var height 42" ) { t.Fail() }
	if ! set_var( vp, "var width 24" ) { t.Fail() }
	if ! set_var( vp, `var title "Now   is   THE time"` ) { t.Fail() }
	vp.Dump_st()

	if ! set_element( vp, `rect	xo:23 yo:45 height:$height width:$width text:"$title"` ) {
		t.Fail()
	}
	
}

/*
	This will load the view from test1.view (and any extensions referenced) and paint
	the view.  Graphs and meters are updated and moved to teest.
*/
func Test_load_view( t *testing.T ) {
	gen_ps = false
	view, _ := Mk_view( "test1.view" )

	gc, _ := get_gc()
	view.Paint( gc, 1.0 )
	gc.Show( )

	time.Sleep( 2 * time.Second )

	us := fmt.Sprintf( `%d`,  20 )	// set the target -- meter should auto decrease with each paint
	view.Data_update( "m1", us )

	for i := 0; i < 60; i++ {
		us := fmt.Sprintf( "update m3  Xo:%d Yo:400", 100 + (10 * i) )
		view.Update( us )


		us = fmt.Sprintf( `%d:%d,%d`, 0, i, 25 + (10 * (i % 7) ) )
		view.Data_update( "graph1", us )

		us = fmt.Sprintf( `1:%d,%d 0:%d,%d`, i, i % 15, i, i % 4 )
		view.Data_update( "graph2", us )

		view.Paint( gc, 1.0 )
		gc.Drive()
		gc.Show( )
		time.Sleep( 40 * time.Millisecond )
	}

	for ;; {
		view.Paint( gc, 1.0 )
		gc.Drive()
		gc.Show( )
		time.Sleep( 10 * time.Millisecond )
	}

	
}
