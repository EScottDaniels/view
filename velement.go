// :vi sw=4 ts=4 noet:
/*
	Memonic:	velement
	Abstract:	Manages the view element struct.
				A view element may be something as a simiple rectangle which 
				translstates directly to an underlying drawing thing that can
				be rendered onto an output device, a complex element (such as a 
				digital clock) which may still need only one underlying drawing
				thing, but require additional support at paint (such as fetching
				the curent time and updating the dthing before painting), 
				or a highly complex element which reqoured multiple drawing things
				to support (such as a meter).
				
	Date:		11 August 2018
	Author: 	E. Scott Daniels
*/

package view

import (
	"bitbucket.org/EScottDaniels/drawingthings"
	"bitbucket.org/EScottDaniels/data_mgr"
	"bitbucket.org/EScottDaniels/sketch"
)

/*
	View element -- something that can be converted into a drawing thing
	and painted using the graphics output mechanisms supported by the graphics
	api. Depending on the element, it could translate into multiple drawing things.
*/
type Velement struct {
	name	string								// the element name for hash based lookup
	xo		float64			// not sure origin coords are important, are they?
	yo		float64
	etype	string
	json	string								// json needed to build the element's drawing thing
	dt		drawingthings.Drawingthing_i		// the paintable thing
	data	*data_mgr.Data_group
}

/*
	Mk_velement creates a view element loacted at the current xo,yo corridant using the 
	given json string. The json is expected to be accepted format defined by the drawingthings
	package.

	Scale is passed to the drawing thing maker should it be needed.
*/
func Mk_velement( ename string, xo float64, yo float64, scale float64, etype string, json string, data *data_mgr.Data_group ) ( ve *Velement, err error ) {
	ve = &Velement {
		name: 	ename,
		xo: 	xo,
		yo:		yo,
		etype:	etype,
		json:	json,
	}

	dt, err := drawingthings.Json2ScaledDt( json, scale )
	if err != nil {
		return nil, err
	}
	ve.dt = dt

	if data != nil {
		ve.data = data			// keep a copy so we can reference it for updates
		dt.Set_data( ve.data )
	}

	return ve, nil
}

/*
*/
func ( ve *Velement ) Contains( x float64, y float64 ) ( bool ) {
	if ve != nil && ve.dt != nil {
		return ve.dt.Contains( x, y )
	}

	return false
}

/*
	Nuke destroys what ever we need to clean up when we're done.
	Must specifically drive underlying dthing cleanup to eliminate
	subpages immediately, not when the GC runs.
*/
func ( ve *Velement ) Nuke( gc sketch.Graphics_api ) {
	if ve != nil {
		ve.dt.Nuke( gc )
		ve.dt = nil
	}
}

/*
	Get_ds returns the element's data group or nil if it doesn't exist.
*/
func ( ve *Velement ) Get_dgroup( ) ( dg *data_mgr.Data_group ) {
	if ve == nil {
		return nil
	}

	return ve.data
}

/*
	Get_name returns the element's name.
*/
func ( ve *Velement ) Get_name( ) ( string ) {
	if ve == nil {
		return ""
	}

	return ve.name
}

/*
	Paint causes this element to be rendered on the output device defined by the graphics
	context (api) passed in.
*/
func ( ve *Velement ) Paint( gc sketch.Graphics_api, scale float64 ) {
	if ve != nil && ve.dt != nil {
		ve.dt.Paint( gc, scale )
	}
}

/*
	Set_scale passes the given scale value to the underlying drawing thing.
*/
func (ve *Velement ) Set_scale( scale float64 ) {
	if ve == nil || ve.dt == nil {
		return
	}

	ve.dt.Set_scale( scale )
}

/*
	Set_iscale passes the given scale value to the underlying drawing thing for 
	an incremental scale change. The increment value is added to the current 
	scale setting.
*/
func (ve *Velement ) Set_iscale( increment float64 ) {
	if ve == nil || ve.dt == nil {
		return
	}

	ve.dt.Set_iscale( increment )
}

/*
	Update causes the passed json string to be used to update the underlying drawing
	thing. Only the values which need to be changed need to exist in the json.
	If a value is not supplied in the json, the current value in the underlying
	drawingthing is unchanged.
*/
func ( ve *Velement ) Update( jbuf string  ) {
	if ve == nil {
		return
	}

	drawingthings.Json_update( ve.dt, jbuf ) 	// pass the json to the drawing thing to update
}

/*
	Data_update accepts a data string which which is passed to the element to update its
	data.  Data varies by element, so the contents of the string are important at the lower
	level and not looked at here.
*/
func ( ve *Velement ) Data_update( data string ) {
	if ve == nil {
		return
	}

	ve.dt.Set_data( data )
}

/*
	A tickle -- likely a button press on a menu
*/
//func (ve *Velement ) Tickle( data string ) {
func (ve *Velement ) Tickle( data interface{} ) {
	if ve == nil {
		return
	}

	ve.dt.Tickle( data )
}

