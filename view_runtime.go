// :vi ts=4 sw=4 noet:
/*
	Mnemonic:	view_runtime
	Abstract:	Functions that support runtime on a view (updates etc.)
	Date:		26 August 2018  (here we go again)
	Author:		E. Scott Daniels
*/

package view

import(
	"fmt"
	"strings"

	"gitlab.com/rouxware/goutils/token"
)

/*
	Update processes an update buffer. We expect:
		update <element-name> key:["]value["]...

	Update is meant to change the dynamics of the dt, size, position, colour, not the data or 
	value.  Use Update_data to change the data.
*/
func ( vp *View ) Update( ibuf interface{} ) {
	var (
		ustr	string
	)

	switch buf := ibuf.(type)  {
		case string:
			ustr = buf
	
		case *string:
			ustr = *buf

		case []byte:
			ustr = string( buf )

		default:
			return 
	}

	ntok, tokens := token.Tokenise_qsep( ustr, " \t" )			// allow space or tabs to separate

	if ntok < 3 {
		return
	}

	vele := vp.ele_by_name[tokens[1]]
	if vele == nil {
		return
	}

	jstr := `{ "Kind":-1`				// kind should not be overlaid, and a -1 will immediatly cause issues if it is
	for i := 2; i < len( tokens ); i++ {
		quote := true
		capture_json := true			// capture the value into the json if not strictly for us

		if tokens[i] != "" {
			etokens := strings.SplitN( tokens[i], ":",  2 )			// split name:value 
			if len( etokens ) != 2 {
				continue
			}

			et := etokens[1]

			if et != "" && strings.IndexAny( et[0:1], "0123456789+-" ) >= 0 {			// if digit, or sign, put in without quotes
				quote = false
			} else {
				if et == "true" || et == "false" {
					quote = false
				} else {
					if len( et ) > 1 && et[0:1] == "\\"  {		// allow \true \false \[-+0-9]
						et = et[1:]
						quote = false;
					}
				}
			}

			switch etokens[0] {				// yank what we care about and for things like x,y alter when needed
				case "data", "Data":		// we don't allow data to pass to the element
					capture_json = false
			}
				
			
			if capture_json {
				if quote {
					jstr += fmt.Sprintf( `, %q: %q`, etokens[0], et )
				} else {
					jstr += fmt.Sprintf( `, %q: %s`, etokens[0], et )
				}
			}
		}
	}

	jstr += " }"

	vele.Update( jstr )
}

/*
	Same as update, but we assume that jstr contains the update json, and elename is the 
	name that we should location. We assume this is invoked when sent a buffer of the form:
		jupdate <name> <json information>
*/
func ( vp *View ) Json_update( elename string, jstr string ) {

	vele := vp.ele_by_name[elename]
	if vele == nil {
		return
	}

	vele.Update( jstr )
}

/*
	Data_pdate processes a change/addition to an element's data. Elements have different data types:
		text - string
		meter - single value
		graph - data group

	We assume the buffer passed is of the form:
		data_update element-name "data-value"

	The data string is passed to the element for processing and each will liklely have a
	different expected format.

	To change the element's properties (position, size, etc.) use the Update function.
*/
func ( vp *View ) Data_update( elename string, ibuf interface{} ) {
	var (
		ustr	string
	)

	switch buf := ibuf.(type)  {		// convert whatever passed into a string
		case string:
			ustr = buf
	
		case *string:
			ustr = *buf

		case []byte:
			ustr = string( buf )

		default:
			return 
	}

	ele := vp.ele_by_name[elename]
	if ele == nil {
		return
	}

	ele.Data_update( ustr )
}


/*
	Tickle an element; probably a menu click.
*/
func ( vp *View ) Tickle(  elename string, ibuf interface{}  ) {
	var (
		ustr	string
	)

	switch buf := ibuf.(type)  {		// convert whatever passed into a string
		case string:
			ustr = buf
	
		case *string:
			ustr = *buf

		case []byte:
			ustr = string( buf )

		default:
			return 
	}

	ele := vp.ele_by_name[elename]
	if ele == nil {
		return
	}

	ele.Tickle( ustr )
}
