// :vi ts=4 sw=4 noet:
/*
	Mnemonic:	view_menu
	Abstract:	Manages menu setup and a goroutine to handle button presses.
	Date:		18 December 2018
	Author:		E. Scott Daniels
*/

package view

import(
	"strings"

	"gitlab.com/rouxware/goutils/clike"
)

type vmenu_ele struct {
	ename	string			// element name/ID that is affected by a button press
	action	string			// the action to send to the element when button is pressed
	colour	string			// colour of the button
	label	string
}

type vmenu struct {
	name	string
	label	string
	colour	string					// default colour, also used to create background colour
	eles	[]*vmenu_ele			// list in order to add to sketch environment
}

/*
	Information about the anchor buttons
*/
type anchor_info struct {
	height	float64					// dimensions of menu anchor buttons
	width	float64
	colour	string				// all buttons have this colour
	x		float64					// position of first button from upper left corner
	y		float64
}

// ---------------------------------------------------------------------------------------------

/*
	Mk_anchor creates anchor info from the tokens passed in.
*/
func mk_anchor_info( tokens []string ) ( ai *anchor_info ) {
	ai = &anchor_info {
		height: 25,
		width: 50,
		colour: "#800080",
		x:		10,
		y:		10,
	}

	for i := 1; i < len( tokens ); i++ {
		stokens := strings.SplitN( tokens[i], ":", 2 )

		parm := "missing"
		if len( stokens ) >= 1 {
			parm = stokens[1]
		}

		switch stokens[0] {
			case "height":
				ai.height = clike.Atof( parm )

			case "width":
				ai.width = clike.Atof( parm )

			case "x":
				ai.x = clike.Atof( parm )

			case "y":
				ai.y = clike.Atof( parm )

			case "colour":
				ai.colour = parm
		}
	}

	return ai
}

// ---------------------------------------------------------------------------------------------

/*
	Mk_vmenu takes an entry from a view file and creates a menu struct that
	can be used to set up an anchored menu. 
		menu	name:<string> color:<string> label:<string>

	We expect tokens to be an array of key:value pairs.

*/
func mk_vmenu( tokens []string ) ( vm *vmenu ) {
	vm = &vmenu {
		eles:	make( []*vmenu_ele, 0, 20 ),			// we use append, so 20 isn't a hard capacity
	}

	for i := 1; i < len( tokens ); i++ {
		stokens := strings.SplitN( tokens[i], ":", 2 )

		parm := "missing"
		if len( stokens ) >= 1 {
			parm = stokens[1]
		}

		switch stokens[0] {
			case "name":
				vm.name = parm

			case "label":
				vm.label = parm

			case "colour":
				vm.colour = parm
		}
	}

	return vm
}

// ----- menu support -----------------
/*
	Get_label returns the label which has been given to the menu.
*/
func ( vp *vmenu ) Get_label( ) ( string ) {
	if vp == nil {
		return ""
	}

	return vp.label
}

/*
	Get_eles retrns the array of elements that make up the menu.
*/
func ( vp *vmenu ) Get_eles( ) ( []*vmenu_ele ) {
	if vp == nil {
		return nil
	}

	return vp.eles
}

/*
	Get_name returns the element name which has been given to the menu.
*/
func ( vp *vmenu ) Get_name( ) ( string ) {
	if vp == nil {
		return ""
	}

	return vp.name
}

// ----- vme support --------------------

/*
	Mk_vmenu_entry will crate a menu entry struct from the tokens passed in.
	The view definition looks like this:

		menu_ele  ename:<string> label:<string> action:<string> colour:<string>

	We expect an array of name:value pairs in tokens. 
*/
func mk_vmenu_entry( tokens []string ) ( vme *vmenu_ele ) {
	vme = &vmenu_ele {
		colour:  "#808080",
		label: "Push-Me",
		action: "reaction",
		ename: "nullandvoid",
	}

	for i := 1; i < len( tokens ); i++ {
		stokens := strings.SplitN( tokens[i], ":", 2 )

		parm := "missing"
		if len( stokens ) > 1 {
			parm = stokens[1]
		}
			
		switch stokens[0] {
			case "action":
				vme.action = parm

			case "ename":
				vme.ename = parm

			case "label":
				vme.label = parm

			case "colour":
				vme.colour = parm
		}
	}

	return vme
}

/*
	Get_label returns the label which has been given to the menu entry.
*/
func ( vme *vmenu_ele ) Get_label( ) ( string ) {
	if vme == nil {
		return ""
	}

	return vme.label
}

/*
	Get_name returns the element name which has been given to the menu entry.
*/
func ( vme *vmenu_ele ) Get_name( ) ( string ) {
	if vme == nil {
		return ""
	}

	return vme.ename
}

/*
	Get_colour returns the colour which has been given to the menu entry.
*/
func ( vme *vmenu_ele ) Get_colour() ( string ) {
	if vme == nil {
		return ""
	}

	return vme.colour
}

/*
	Get_action returns the action which has been given to the menu entry.
*/
func ( vme *vmenu_ele ) Get_action() ( string ) {
	if vme == nil {
		return ""
	}

	return vme.action
}
