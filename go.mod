module bitbucket.org/EScottDaniels/view

go 1.15

require (
	bitbucket.org/EScottDaniels/data_mgr v1.1.0
	bitbucket.org/EScottDaniels/drawingthings v1.3.1
	bitbucket.org/EScottDaniels/menu v1.2.0
	bitbucket.org/EScottDaniels/sketch v1.2.0
	github.com/ScottDaniels/gopkgs v1.0.2 // indirect
	gitlab.com/rouxware/goutils/clike v1.0.0
	gitlab.com/rouxware/goutils/token v1.0.0
)
