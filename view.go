// :vi ts=4 sw=4 noet:
/*
	Mnemonic:	view
	Abstract:	A view is a collection of drawing things described in a view file.
	Date:		11 August 2018  (here we go again)
	Author:		E. Scott Daniels
*/

package view

import(
	"bufio"
	"fmt"
	"os"
	"strings"

	"gitlab.com/rouxware/goutils/clike"
	"bitbucket.org/EScottDaniels/drawingthings"
	"bitbucket.org/EScottDaniels/sketch"
	sktools "bitbucket.org/EScottDaniels/sketch/tools"
	"bitbucket.org/EScottDaniels/menu"
)

var (
	type2dt_kind	map[string]int		// maps our types to the drawingthing kind needed in json 
)

type View struct {
	fname	string						// file name containing the view
	veles	[]*Velement					// the set of elements in drawing order
	ele_by_name	map[string]*Velement	// name reference
	menus	[]*vmenu					// any defined menus
	bid2vme map[int]*vmenu_ele			// xlates menu button IDs to view elements and actions
	menus_shown bool					// we build and push menus out during first call to paint()
	menu_set *anchor_info				// describes the menu 'anchor' buttons
	scale	float64						// scale the view is created with
}

type Open_file struct {
	next	*Open_file
	f		*os.File	
	br		*bufio.Reader
	fname	string
	line	int
}

/*
	Package level initialisation run once at programme start.
*/
func init( ) {
	type2dt_kind = make( map[string]int, 17 )
	
	type2dt_kind["rect"] = drawingthings.DT_RECT			// init our translation table
	type2dt_kind["text"] = drawingthings.DT_TEXT
	type2dt_kind["line"] = drawingthings.DT_LINE
	type2dt_kind["clock"] = drawingthings.DT_CLOCK
	type2dt_kind["barg"] = drawingthings.DT_BARGRAPH
	type2dt_kind["rung"] = drawingthings.DT_RUNGRAPH
	type2dt_kind["tangle"] = drawingthings.DT_TANGLE
	type2dt_kind["circle"] = drawingthings.DT_CIRCLE
	type2dt_kind["piechart"] = drawingthings.DT_PIECHART
	type2dt_kind["regpoly"] = drawingthings.DT_REGPOLY
	type2dt_kind["meter"] = drawingthings.DT_METER
	type2dt_kind["bargraph"] = drawingthings.DT_BARGRAPH
	type2dt_kind["rungraph"] = drawingthings.DT_RUNGRAPH
	type2dt_kind["image"] = drawingthings.DT_IMAGE
	type2dt_kind["blip"] = drawingthings.DT_BLIP
}

/*
	Convenience function to translate a view record element type into 
	a drawingthing kind.
*/
func rtype2dt( rtype string ) ( int ) {
	return type2dt_kind[rtype]
}

/*
	Open the named file and put it on the list. The pointer to the new head
	of the list is returned.  If a file cannot be opened, an error message
	is writte, and the old list is returned.
*/
func open_file( flist *Open_file, fname string ) ( nl *Open_file ) {
	
	f, err := os.Open( fname )			// open read only
	if err != nil {
		fmt.Fprintf( os.Stderr, "unable to open file: %s :%s\n", fname, err )
		return flist
	}

	nl = &Open_file {
		f: 	f,
    	br: bufio.NewReader( f ),
		fname:	fname,
	}

	if flist == nil {
		return nl
	} 

	nl.next = flist
	flist = nl
	return flist
}

/*
	Deprecated!
	Original form (prevent scaled support from being breaking change). Defaults to a scale of 1.0.
	Use MkScaledView( fname, scale)
*/
func Mk_view( fname string ) ( v *View, err error ) {
	return MkScaledView( fname, 1.0 )
}

/*
	Read the view file from fname and create a view. We need the scale
	because some drawing things which create a subpage must scale that
	outside of the GC.
*/
func MkScaledView( fname string, scale float64 ) ( v *View, err error ) {
	var (
		flist	*Open_file
		last_menu *vmenu
	)

	trace := false
	flist = open_file( flist, fname )
	if flist == nil {
		return nil, fmt.Errorf( "bad file?" )
	}

	v = &View {
		veles: 			make( []*Velement, 0, 1024 ),		// default to 1K; append will grow if needed
		menus:			make( []*vmenu, 0, 16 ),
		ele_by_name:	make( map[string]*Velement, 1027 ),
		bid2vme:		make( map[int]*vmenu_ele ),
		scale:			1.0,
	}

	vp := Mk_vparser( scale )
	for ; flist != nil ; {			// until we've processed the stack
		flist.line++

       	rrec, rerr := flist.br.ReadString( '\n' );
		rec := ""
		for ; len( rrec ) > 2 && rerr == nil && rrec[len(rrec)-2:] == "\\\n"; {		// allow escaped newlines
			rec += rrec[0:len( rrec ) -2] + " "
       		rrec, rerr = flist.br.ReadString( '\n' );
		}
		rec += rrec
		
       	if rerr == nil {
			rec = strings.TrimSpace( rec )
			kind, thingi := vp.Crunch_rec( rec )
			switch kind {
				case "\n", "":										// blank; do nothing, parser handled completely (calc, trace, etc)

				case "extend":
					fname, ok := thingi.(string)
					if trace {
						fmt.Fprintf( os.Stderr, "extending view with: %s\n", fname ) 
					}
					if ok {
						if trace {
							fmt.Fprintf( os.Stderr, "extending view with: %s\n", fname ) 
						}
						flist = open_file( flist, fname )		// open extension and push onto the list
					} else {
						fmt.Fprintf( os.Stderr, "%s: %d: extend filename bad or missing\n", flist.fname, flist.line )
					}

				case "menu_set":
					ai, ok := thingi.( *anchor_info )
					if ok {
						v.menu_set = ai
					}
					
				case "menu":
					m, ok := thingi.( *vmenu ) 
					if ok {
						v.menus = append( v.menus, m )
						last_menu = m
					}

				case "menu_ele":
					vme, ok := thingi.( *vmenu_ele )
					if ok {
						if last_menu != nil {
							last_menu.eles = append( last_menu.eles, vme )
						}
					}

				case "traceon":
					trace = true

				case "traceoff":
					trace = false


				default:
					vele, ok := thingi.( *Velement )				// some kind of element, stash in the array and hash table
					if ok {
						v.veles	= append( v.veles, vele )
						v.ele_by_name[vele.Get_name()] = vele
					}
					
					//fmt.Fprintf( os.Stderr, "%s: %d: unable to handle record: %s\n", flist.fname, flist.line, kind )
			}
		} else {							// assume end of active file
			if trace {
				fmt.Fprintf( os.Stderr, "closing extension: %s\n", flist.fname ) 
			}
			flist.f.Close( )				// close and move to next if there
			flist = flist.next
		}
	}

	return v, nil
}

/*
	Drives our side of menu triggered events. We have an instance of the driver 
	started for each menu -- mostly because we need to know which memnu triggered
	the event in the case that the menu is to be hidden after execution. 
*/
func ( vp *View ) menu_driver(  gc sketch.Graphics_api, mm *menu.Menu_mgr, mname string, echan chan *menu.Mevent ) {
	if vp == nil {
		return
	}

	// until the cows come home, pigs fly, or adult childrern move out of the house
	for ;; {
		e := <- echan			// button press generaets events; wait patiently

		bid := e.Bid
		vme := vp.bid2vme[bid]
		if vme == nil {
			continue
		}

		switch vme.action {
			case "close": 					// some actions are interpreted here...
				mm.Hide_menu( mname )

			case "home":
				gc.Translate( float64( 0 ), float64( 0 ) )

			case "horiz_scroll":
				amt := clike.Atoi( vme.ename )			// ele 'name' is the amount
				gc.Translate_delta( float64( amt), float64( 0 ) )

			case "quit":
				os.Exit( 0 )

			case "iscale":								// incremental scale 
				amt := clike.Atof( vme.ename )			// ele 'name' is the amount
				for _, ele := range vp.ele_by_name {
					if ele != nil {
						ele.Set_iscale( float64( amt ) )
					}
				}	
				gc.Set_iscale( float64( amt ) )
		

			case "scale":
				amt := clike.Atof( vme.ename )			// ele 'name' is the amount
				if( amt > 0.0 ) {
					for _, ele := range vp.ele_by_name {
						if ele != nil {
							ele.Set_scale( float64( amt ) )
						}
					}	
					gc.Set_scale( float64( amt ), float64( amt ) )
				}
			
			case "vert_scroll":
				amt := clike.Atoi( vme.ename )			// ele 'name' is the amount
				gc.Translate_delta( float64( 0), float64( amt ) )
		
			default:				// assume we need to tickle the view element
				ename := vme.ename
				if ename == "=all" {				// tickle all elements
					for _, ele := range vp.ele_by_name {
						if ele != nil {
							ele.Tickle( vme.action )
						}
					}	
				} else {
					ele := vp.ele_by_name[ename]
					if ele != nil {
						ele.Tickle( vme.action )
					}
				}
		}
	}
}

/*
	Start_drivers pushes all menu definitions out to the underlying sketch environment
	and creates the map that allows button IDs to be translated to element/actions.
	It then listens to the channel for events and causes the element to be ticked
	when the corresponding button is pressed.	Because all buttons in sketch have
	a unique ID, we can handle all presses from a single function.
*/
func ( vp *View ) start_drivers( gc sketch.Graphics_api ) {
	if vp == nil || len( vp.menus ) < 1 {
		return
	}


	//mm := menu.Mk_menu_set( gc, 10, 10, 25, 50, "#800080", "white", false )
	mm := menu.Mk_menu_set( gc, vp.menu_set.x, vp.menu_set.y, vp.menu_set.height, vp.menu_set.width, vp.menu_set.colour, "white", false )
	for i := 0; i < len( vp.menus ); i++  {
		echan := make( chan *menu.Mevent, 128 )
		name := vp.menus[i].Get_name()
		mm.Add_anchor( name, vp.menus[i].Get_label(), 25, 256, "#000080", echan )

		elist := vp.menus[i].Get_eles()
		kind := sketch.BUTTON_SPRING
		for j := 0; j < len( elist ); j++  {
			bid := mm.Add_button( name, elist[j].Get_label(), elist[j].Get_colour(), "white", kind, false, echan )
			vp.bid2vme[bid] = elist[j]
		}

		go vp.menu_driver( gc, mm, name, echan )
	}
}

/*
	Paint renders all view elements, in order, using the graphics context
	(sketch) passed in. We assume that the underlying graphics medium is 
	double buffered and thus needs to have every element drawn on each call,
	not just those who think they've changed.
*/
func ( vp *View ) Paint( gc sketch.Graphics_api, scale float64 ) {
	if vp == nil || gc == nil {
		return
	}

	if ! vp.menus_shown {
		vp.start_drivers( gc ) 			// configure the menus in sketch and start the drivers
		vp.menus_shown = true
	}

	for _, ele := range vp.veles {
		ele.Paint( gc, scale )
	}
}

/*
*/
func ( vp *View ) Hard_click( ev *sktools.Interaction ) {
	if vp == nil || ev == nil {
		return
	}

	if ev.Kind != sktools.IK_M_RELEASE {		// we only trap releases
		return
	}

	scaled_x := ev.X * (1/vp.scale)
	scaled_y := ev.Y * (1/vp.scale)
	for _, ele := range vp.veles {
		if ele.Contains( scaled_x, scaled_y ) {
			ele.Tickle( ev )
			return
		}
	}
}

/*
	Will set the scale for all elements in the view. Scale for images/graphs
	does affect size while for most other elements it only affects visibility
	if an element is painted only when at a certain scale setting.
*/
func ( vp *View ) Set_scale( scale float64 ) {
	if vp == nil {
		return
	}

	vp.scale = scale

	for _, ele := range vp.ele_by_name {
		if ele != nil {
			ele.Set_scale( float64( scale ) )
		}
	}	
}
