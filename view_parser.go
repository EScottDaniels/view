
/*
	Memonic:	view_parser
	Abstract:	Manages a parser struct and provides functions to parse records
				from a view file.
	Date:		11 August 2018
	Author: 	E. Scott Daniels
*/


package view

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/rouxware/goutils/clike"
	"gitlab.com/rouxware/goutils/token"
	"bitbucket.org/EScottDaniels/drawingthings"
	"bitbucket.org/EScottDaniels/data_mgr"
)

/*
	Used to save/restore the state.
*/
type vparser_state struct {
	vars	map[string]string		// variable symbol table
	xo		float64					// origin coords
	yo		float64
}

type Vparser struct {
	vars	map[string]string		// variable symbol table
	xo		float64					// origin coords
	yo		float64
	scale	float64					// the current scale
	trace	bool					// detailed parsing output when true
	stack	[]*vparser_state		// stack of saved states
	stack_idx int
}

// ------------------------------------------------------------------------------

/*
	Given a map, add our supplied "system" variables (mostly drawing thing
	constants).
*/
func add_sys_vars( vars map[string]string ) {
	vars["_default"] = 	fmt.Sprintf( "%d", drawingthings.XL_DEFAULT )
    vars["_xl_reverse"] = fmt.Sprintf( "%d", drawingthings.XL_REVERSE )
    vars["_xl_index"] = fmt.Sprintf( "%d", drawingthings.XL_INDEX         )
    vars["_xl_rev_index"] = fmt.Sprintf( "%d", drawingthings.XL_REV_INDEX    )
    vars["_xl_time"] = fmt.Sprintf( "%d", drawingthings.XL_TIME        )
    vars["_xl_rev_time"] = fmt.Sprintf( "%d", drawingthings.XL_REV_TIME   )
    vars["_xl_timehm"] = fmt.Sprintf( "%d", drawingthings.XL_TIME_HM   )
    vars["_xl_rev_timehm"] = fmt.Sprintf( "%d", drawingthings.XL_REV_TIME_HM )
    vars["_xl_time_h"] = fmt.Sprintf( "%d", drawingthings.XL_TIME_H   )
    vars["_xl_rev_timeh"] = fmt.Sprintf( "%d", drawingthings.XL_REV_TIME_H )
    vars["_xl_date"] = fmt.Sprintf( "%d", drawingthings.XL_DATE )
    vars["_xl_rev_date"] = fmt.Sprintf( "%d", drawingthings.XL_REV_DATE )
    vars["_xl_date_time"] = fmt.Sprintf( "%d", drawingthings.XL_DATE_TIME )
    vars["_xl_rev_date_time"] = fmt.Sprintf( "%d", drawingthings.XL_REV_DATE_TIME )

    vars["_clock_hms"] = "13:04:05"							// clock format strings
    vars["_clock_hm"] = "13:04"
    vars["_clock_yyyymmdd"] = "2006/01/02"
    vars["_clock_date"] = "_2 Jan 2006"
    vars["_clock_date_time"] = "_2 Jan 2006 15:04:05"

    vars["_needle_tic"] = fmt.Sprintf( "%d", drawingthings.Needle_tic )
    vars["_needle_full"] = fmt.Sprintf( "%d", drawingthings.Needle_full )

    vars["_meter_220"] = fmt.Sprintf( "%d", drawingthings.Meter_220 )
    vars["_meter_180"] = fmt.Sprintf( "%d", drawingthings.Meter_180 )
    vars["_meter_90"] = fmt.Sprintf( "%d", drawingthings.Meter_90 )
}

/*
	Given a buffer with a data group description (nsets,set_size,colour0,colour1...,colourn)
	create a data group and return it.
*/
func mk_data( buf string ) ( dg *data_mgr.Data_group ) {
	tokens := strings.Split( buf, "," )
	if len( tokens ) < 3 {
		fmt.Printf( ">>> bad data specification for graph: expected nsets,set_size,colour0,colour1...,colourn\n" )
		return
	}

	return data_mgr.Mk_data_group( clike.Atoi( tokens[0] ), clike.Atoi( tokens[1] ), tokens[2:] )
}

// ------------------------------------------------------------------------------

/*
	Mk_vparser creates the parser struct which is needed to parse or get
	information about the parsed data.
*/
func Mk_vparser( scale float64 ) ( vp *Vparser ) {
	
	vp = &Vparser{
		vars: make( map[string]string, 101 ),
		stack: make( []*vparser_state, 64 ),
		scale: scale,
	}

	add_sys_vars( vp.vars )

	return vp
}

/*
	Given a value and an assignment operator, apply the operator using the
	current variable value (if appropriate) and save the resulting value.
*/
func ( p *Vparser ) apply_assop( assop string, vname string, value float64 ) {
	switch assop {
		case "=":
			p.vars[vname] = fmt.Sprintf( "%.4f", value )

		case "+=":
			p.vars[vname] = fmt.Sprintf( "%.4f", clike.Atof( p.vars[vname] ) + value )

		case "-=":
			p.vars[vname] = fmt.Sprintf( "%.4f", clike.Atof( p.vars[vname] ) - value )

		case "*=":
			p.vars[vname] = fmt.Sprintf( "%.4f", clike.Atof( p.vars[vname] ) * value )

		case "/=":
			if value != 0 {
				p.vars[vname] = fmt.Sprintf( "%.4f", clike.Atof( p.vars[vname] ) / value )
			}
	}
}

/*
	Compute accepts a variable name, assignment operator, and expression 
	tokens.  The expression tokens are evaluated as a reverse polish
	expression (e.g.  1 2 3 + +  or  1 2 * 4 + 6 /) and the result is
	stored into the named variable using the assignment operator:
		 = direct replacement
		+= add to current value
		-= subtract from current value
		*= current value is multipled by result
		/= current value is divided by result if result !0

	Tokens are expanded, so it is not necessary to expand prior to calling.
	Any invalid number is treated as a 0. If an expression resulted in zero 
	tokens, then a 0 is applied per the assignment operator.
*/
func ( p *Vparser ) Compute( vname string, ass_op string, tokens []string ) {
	stack := make( []float64, 1024 )
	si := 0

	ntoks := len( tokens )
	if ntoks <= 0 {
		p.apply_assop( ass_op, vname, 0.0 )
		return
	}

	for i :=  0; i < ntoks; i++ {
		tokens[i] = p.Expand( tokens[i] )

		if tokens[i] == "" {			// allow empty tokens
			continue
		}

		if tokens[i][0:1] == "#" {		// allow a comment to end parsing if not removed
			break
		}

		switch tokens[i] {
			case "":					// allow nil token

			case "+":
				if si > 1 {
					si--
					stack[si-1] = stack[si-1] + stack[si]
				}

			case "-":
				if si > 1 {
					si--
					stack[si-1] = stack[si-1] - stack[si]
				}

			case "*":
				if si > 1 {
					si--
					stack[si-1] = stack[si-1] * stack[si]
				}

			case "/":
				if si > 1 {
					si--
					if stack[si] != 0 {
						stack[si-1] = stack[si-1] / stack[si]
					} else {
						stack[si-1] = 0
					}
				}

			case "%":
				if si > 1 {
					si--
					if stack[si] != 0 {
						stack[si-1] = float64( int( stack[si-1] ) % int( stack[si] ) )
					} else {
						stack[si-1] = 0
					}
				}

			default:
				//fmt.Printf( "[%d] Push: %.2f\n", i, clike.Atof( tokens[i] ) )
				if si < len( stack ) - 1 {
					stack[si] = clike.Atof( tokens[i] )
					si++
				}
		}
	}

	if si == 0 {
		stack[si] = 0
		si++
	}

	p.apply_assop( ass_op, vname, stack[si-1] )
}

/*
	Expand_tokens ccepts a slice of string pointers and builds an expanded
	string. Comments (anything after a token with a single hash (#) are
	removed.
*/
func ( p *Vparser ) Expand_tokens( tokens []string ) ( string ) {
	estr := ""
	sep := ""
	for  i := range tokens {						// bang tokens together with single space
		if tokens[i] == "#"	{						// comments MUST have trailing space to allow for #rrggbb things
			break
		}

		if tokens[i] != "" {
			estr += sep + tokens[i]
			sep = " "
		}
	}

	return p.Expand( estr )
}

/*
	Expand searches a string for any $name or ${name} strings and expand with
	the current value in the symbol tab.  We allow the following derefernce syntax:
		$vname
		${vname}[junk]   (junk not treated as part of the name, and is appended to the expansion
		${vname:-default}

	Var references s must be separated by space or tab, or the name must be wrapped
	in curly braces.
*/
func ( p *Vparser ) Expand( buf string ) ( ebuf string ) {
	if p == nil || len( buf ) < 2 {
		return buf
	}


	nxti := strings.Index( buf, "$" )
	if nxti < 0 {
		return buf
	}

	prefix := ""
	if nxti > 0 && buf[nxti-1:nxti] == "\\" {
		prefix = buf[0:nxti-1] + "$"
		if nxti >= len( buf ) {
			return prefix + buf
		}

		buf = p.Expand( buf[nxti+1:] )
	} else {
		if nxti > 0 {
			prefix = buf[0:nxti]					// everything before the $
		}
		buf = buf[nxti+1:]
		vname := ""
		endi := 0

		if buf[0:1] == "{" {	
			endi = strings.Index( buf, "}" )			// find }
			vname = buf[1:endi]
			endi++										// bump to skip }
		} else {
			endi = strings.IndexAny( buf, " \t" )		// find separator; don't bump endi so that we include sep
			if endi < 0 {
				vname = buf
			} else {
				vname = buf[0:endi]
			}
		}

		didx := strings.Index( vname, ":-" )
		def_val := ""
		if didx >= 0 {
			if didx < len( vname ) - 2 {
				def_val = vname[didx+2:]
			}
			vname = vname[0:didx]
			val := p.vars[vname ]
			if val != "" {
				prefix += val
			} else {
				prefix += def_val
			}
		} else {
			prefix += p.vars[vname]
		}

		if endi > 0  &&  endi+1 < len( buf ) {
			buf = p.Expand( buf[endi:] )
		} else {
			buf = ""
		}
	}

	return prefix + buf
}


/*
	Crunch a record from the view input. Returns record type and json if it's
	an element.
	View record has the format:
		<type> <key>:["]value["]

		var <vname> <value>
		orig <x>,<y>
		delta <delta-x>,<delta-y>

		Variables are substituted in values using $vname, ${vname} or ${vname:-default}
		syntax.

	Returns the record type and a pointer to the resuting data struct (Velement, or string)
	as an interface.
*/
func ( p *Vparser ) Crunch_rec( ibuf interface{} ) ( rtype string, result interface{} ) {
	var (
		ve *Velement
		err error
		data *data_mgr.Data_group
	)

	if p == nil {
		return "bad-ptr", nil
	}

	result = nil
	vstr := ""
	switch buf := ibuf.(type)  {
		case string:
			vstr = buf
	
		case *string:
			vstr = *buf

		case []byte:
			vstr = string( buf )

		default:
			return "bad-data", nil
	}

	ntok, tokens := token.Tokenise_qsep( vstr, " \t" )			// allow space or tabs to separate

	i := 0
	for i = 0; i < ntok; i++ {
		if tokens[i] != "" {
			break
		}
	}

	if i >= ntok {
		return "", ""
	}

	tokens = tokens[i:]			// ditch  leading nil tokens


	if tokens[0][0:1] == "#" {
		return "comment", nil
	}

	rtype = tokens[0]
	switch tokens[0] {
		case "comp", "compute", "calc", "eval":					// expect vname [-|+]= expression
			if len( tokens ) > 4 {
				vname := p.Expand( tokens[1] )
				p.Compute( vname, tokens[2], tokens[3:] )		// compute and save the value

				if p.trace {
					fmt.Fprintf( os.Stderr, "computed: %s = %sf\n", tokens[1], p.vars[tokens[1]] )
				}
			}
			
		case "delta", "deltaorig":							// given values are added to the current setting
			if ntok > 1 {
				if( ntok > 1 ) {
					p.xo += clike.Atof( p.Expand( tokens[1] ) )
				}
				if( ntok > 2 ) {
					p.yo += clike.Atof( p.Expand( tokens[2] ) )
				}
				if p.trace {
					fmt.Fprintf( os.Stderr, "orig set with delta: %.2f,%.2f\n", p.xo, p.yo )
				}
			}

		case "dump":												// dump the symbol table
			p.Dump_st()

		case "extend":												// imbed another veiw file
			if ntok > 1 {
				result = p.Expand( tokens[1] )
			}

		case "menu":
			if ntok > 1 {
				for i := range tokens {
					tokens[i] = p.Expand( tokens[i] )
				}
				return "menu", mk_vmenu( tokens ) 
			}

		case "menu_ele":
			if ntok > 1 {
				for i := range tokens {
					tokens[i] = p.Expand( tokens[i] )
				}
				return "menu_ele", mk_vmenu_entry( tokens ) 
			}

		case "menu_set":				// define anchor info for the set
			if ntok > 1 {
				for i := range tokens {
					tokens[i] = p.Expand( tokens[i] )
				}
				return "menu_set", mk_anchor_info( tokens )
			}

		case "orig", "origin":										// set the origin (translate)
			if( ntok > 1 ) {
				p.xo = clike.Atof( p.Expand( tokens[1] ) )
			} else {
				p.xo = 0.0
			}
			if( ntok > 2 ) {
				p.yo = clike.Atof( p.Expand( tokens[2] ) )
			} else {
				p.yo = 0
			}
			if p.trace {
				fmt.Fprintf( os.Stderr, "orig set: %.2f,%.2f\n", p.xo, p.yo )
			}

		case "restore":
			if p.trace {
				fmt.Fprintf( os.Stderr, "state restored\n" )
			}
			p.Restore()

		case "save":
			if p.trace {
				fmt.Fprintf( os.Stderr, "state saved\n" )
			}
			p.Save()

		case "trace":
			if ntok < 2 || tokens[1] == "on" {
				p.trace = true
				result = "traceon"							// so parent can trace if they want
			} else {
				p.trace = false
				result = "traceoff"
			}

		case "var":											// expect var key value tokens [# comment]
			if ntok > 1 {
				vname := tokens[1]
				val := p.Expand_tokens( tokens[2:] )
				p.vars[vname] = val
				if p.trace {
					fmt.Fprintf( os.Stderr, "var set: %s = %sf\n", tokens[1], p.vars[tokens[1]] )
				}
			}

		default:															// assume an element type
																			// convert key:value things into json, syphoning off any bits we care about	

			if ntok < 2 {
				break
			}

			jstr := fmt.Sprintf( `{ "Kind": %d`, rtype2dt( rtype ) )		// add the drawing thing kind

			xo := p.xo
			yo := p.yo
			ename := "unknown"
			data = nil
			found_id := false					// set to true if user supplied an id; if not noticed; we dup name to id for dthings

			for i := 1; i < len( tokens ); i++ {
				//fmt.Printf( ">>> tokens-len=%d token[%d]=(%s)\n", len(tokens), i, tokens[i] )
				quote := true
				capture_json := true			// capture the value into the json if not strictly for us

				if tokens[i] != "" {
					etokens := strings.SplitN( tokens[i], ":",  2 )			// split name:value and expand value token
					et := "no-value"
					if len( etokens ) > 1 {
						et = p.Expand( etokens[1] )
					}

					if et != "" {
						if strings.IndexAny( et[0:1], "0123456789+-" ) >= 0 {			// if digit, or sign, put in without quotes
							quote = false
						} else {
							if et == "true" || et == "false" {
								quote = false
							} else {
								if et[0:1] == "\\" {		// allow \true \false \[-+0-9]
									et = et[1:]
								}
							}
						}
					}

					switch etokens[0] {				// yank what we care about and for things like x,y alter when needed
						case "Id", "id":
							found_id = true

						case "Name", "name":
							ename = et

						case "x", "X", "xo", "Xo":
							xo += clike.Atof( et )
							et = fmt.Sprintf( "%.2f", xo )

						case "y", "Y", "yo", "Yo":
							yo += clike.Atof( et )
							et = fmt.Sprintf( "%.2f", yo )

						case "data", "Data":			// allocate a data group for the dt; value is nsets,set_size,colour(s)
							capture_json = false		// we don't pass this info
							data = mk_data( et )
					}
						
					
					if capture_json {
						if quote {
							jstr += fmt.Sprintf( `, %q: %q`, etokens[0], et )
						} else {
							jstr += fmt.Sprintf( `, %q: %s`, etokens[0], et )
						}
					}
				}
			}

			if ! found_id {
				jstr += fmt.Sprintf( `, "Id": %q }`, ename )
			} else {
				jstr += " }"
			}

			if p.trace {
				fmt.Fprintf( os.Stderr, "creating element: %s (%.2f,%.2f)\n", ename, xo, yo )
			}

			ve, err = Mk_velement( ename, xo, yo, p.scale, rtype, jstr, data )
			if err != nil {
				fmt.Fprintf( os.Stderr, "failed to make view element type=%s: %s\n", rtype, err )
				fmt.Fprintf( os.Stderr, "json: (%s)\n", jstr )
				return rtype, nil
			} else {
				result = ve
			}
	}

	return rtype, result
}

/*
	Save will save the current state and push a copy of the parser onto the stack.
	A "depth" of 64 states may be pushed.
*/
func ( p *Vparser ) Save( ) {
	if p == nil || p.stack_idx >= len( p.stack ) {
		return
	}

	state := &vparser_state{
		xo:	p.xo,
		yo:	p.yo,
		vars:	p.vars,
	}

	p.vars = make( map[string]string, 1024 ) 		// copy the map so that when restore all vars created during are removed
	for k, v := range state.vars {
		p.vars[k] = v
	}

	p.stack[p.stack_idx] = state
	p.stack_idx++
}

/*
	Restore will 'pop' the saved state (if one exists) reruning all parser variables, and the symbol table
	to the state which existed when save was called.
*/
func ( p *Vparser ) Restore() {
	if p == nil || p.stack_idx <= 0 {
		return
	}

	p.stack_idx--
	state := p.stack[p.stack_idx]
	p.stack[p.stack_idx] = nil
	p.xo = state.xo
	p.yo = state.yo
	p.vars = state.vars
	state = nil
}

/*
	Get origin returns the current x,y coordinates of the origin.
*/
func ( p *Vparser ) Get_origin( ) ( x float64, y float64 ) {
	if p == nil {
		return 0.0, 0.0
	}

	return p.xo, p.yo
}


/*
	Dump_st spits the parser's symbol table out to standard error.
*/
func ( p *Vparser ) Dump_st( ) {
	fmt.Fprintf( os.Stderr, "There are a total of %d variables in the symbol table, showing only user variables\n", len( p.vars ) )
	if p == nil {
		return
	}

	for k, v := range p.vars {
		if k[0:1] != "_" {
			fmt.Fprintf( os.Stderr, "var: %s = %s\n", k, v )
		}
	}
}
